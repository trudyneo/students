import random

class Game:

    def __init__(self):
        self.player_score = 0
        self.computer_score = 0
        self.player_choice = None # Will be set to either 0, 1 or 2 depending on whether r, p or s is the user input
        self.computer_choice = None # Will be randomly set to either 0, 1 or 2
        self.choices = ["rock", "paper", "scissors"] # Available choices in this order since 0 corresponds to rock, 1 to paper etc...
        self.round = 1 # Game will be endless rounds, only stopping when the user quits
    
    # Function to display the main menu
    def main_menu(self):
        print("\n-------------------")
        print("ROCK PAPER SCISSORS")
        print("-------------------")
        print("PLAY          -  P")
        print("INSTRUCTIONS  -  I")
        print("QUIT          -  Q")

    # Function to check the user's input to the main menu screen and, if valid, returns their choice
    def get_main_menu_input(self):
        while True:
            choice = input("Enter P, I or Q: ").lower().strip()
            if len(choice) != 1 or choice not in "piq":
                print("Invalid input!")
            else:
                return choice
    
    # Function to display the instructions and the user continues to play the game after pressing the enter key
    def instructions(self):
        print("\n------------")
        print("INSTRUCTIONS")
        print("------------")
        print("When prompted:")
        print("Type 'r' for rock")
        print("Type 'p' for paper")
        print("Type 's' for scissors")
        print("Type 'q' to quit")
        print("Press the Enter key to submit your choice")

        input("\nPress Enter to start playing ")
        self.play_game()

    # Validates the user's input and returns the number which corresponds to their choice if valid
    def get_player_choice(self):
        while True:
            # Takes their input as a lowercase string with white space on either end of the string removed
            player_choice = input("Enter (r)ock, (p)aper or (s)cissors: ").lower().strip()

            if player_choice.startswith("r"): # If user types something beginning with "r" it is assumed they picked rock
                # 0 corresponds to rock
                return 0
            elif player_choice.startswith("p"):
                return 1
            elif player_choice.startswith("s"):
                return 2
            elif player_choice.startswith("q"): # If user types something beginning with "q" it is assumed they want to quit the game
                self.quit_game() # Calls the class' quit_game function
            else: # Otherwise user input must be invalid
                print("Invalid input!")
    
    # Function to randomly generate the computer's choice and display to the user
    def get_computer_choice(self):
        # Random number between 0 and 2 inclusive generated
        self.computer_choice = random.randint(0, 2)
        # Displays the string form of the choice through array indexing
        print("\nComputer plays", self.choices[self.computer_choice])

    # Fucntion to determine the winner of the round, award points (if necessary) and increment the round number
    def get_round_winner(self):
        # Takes the numerical difference between the player's choice and the computer's choice
        difference = self.player_choice - self.computer_choice

        if difference == 0: # A difference of 0 suggests both chose the same thing
            print("\nTie!") # Therefore, it's a tie
        elif difference % 3 == 1: # If the difference mod 3 is 1
            print("\nYou win!") # This suggests the player won
            self.player_score += 1 # Player's score increments
        else: # Otherwise, since the player and computer hasn't tied, nor has the player won
            print("\nComputer wins!") # The computer must have won by default
            self.computer_score += 1 # Computer's score increments
        
        self.round += 1 # Round number increments

    # Function for the gameplay
    def play_game(self):
        # Infinite loop which is only exit once the player quits the game
        while True:
            # Displays round number
            print()
            print("-" * len("ROUND " + str(self.round)))
            print("ROUND", self.round)
            print("-" * len("ROUND " + str(self.round)))

            # Player choice is equal to the return of the class' get_player_choice function
            self.player_choice = self.get_player_choice()
            # Computer choice is set to be a random value through the use of the class' get_computer_choice function
            self.get_computer_choice()
            
            # Function called to calculate winner
            self.get_round_winner()
            # Score is displayed after the round is complete
            self.show_score()

    # Function to display the overall winner from all rounds played (even if there are 0 rounds played)
    def get_final_winner(self):
        if self.player_score == self.computer_score: # If the player's score is equal to the computer's score
            print("\nAt least you didn't lose...") # They have tied
        elif self.player_score < self.computer_score: # If the player's score is less than the computer's score
            print("\nPlay better next time") # They have lost
        else: # Otherwise
            print("\nCongrats!") # The player wins by process of elimination
    
    # Function to display the score using string formatting
    def show_score(self):
        print("\nYou: {0}".format(self.player_score))
        print("Computer: {0}".format(self.computer_score))
    
    # Function that shows the final score, displays result of the final score and quits the program
    def quit_game(self):
        print("\n-----------")
        print("FINAL SCORE")
        print("-----------")
        self.show_score()
        self.get_final_winner()
        quit()

    # Function that handles the entire game loop
    def game_loop(self):
        # Displays main menu
        self.main_menu()
        # Gets the user input for the main menu
        choice = self.get_main_menu_input()
        
        if choice == "q": # If the user types something starting with "q"
            self.quit_game() # The class' quit_game function is called
        elif choice == "i": # If the user types something starting with "i"
            self.instructions() # The class' instructions function is called
        else: # Otherwise
            self.play_game() # The user wants to play a game

# Game object instantiated
game = Game()
# game_loop function from the Game class is called through the object
game.game_loop()