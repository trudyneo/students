// gcc -o PriorityQueue PriorityQueue.c
// ./PriorityQueue

#include <stdio.h>
#include <stdbool.h>

// Define maximum queue size
#define MAX_QUEUE_SIZE 5

// ========================================
// Struct for the data items
// Holds their value and their level of
// priority (1 - lowest; 5 - highest)
// ========================================
typedef struct
{
    char value;
    int priority;
} dataItem;

// ========================================
// Generic zero data item to act as a
// placeholder for empty elements of the
// array
// ========================================
dataItem zero_data = {'-', 0};

// Declare global array to hold queue items
dataItem queue[MAX_QUEUE_SIZE];

// Declare queue markers
int front;
int back;

// Declare variable for queue length
int queue_len;

// ========================================
// Initialise queue to all zeros
// Initialise queue markers to zero
// ========================================
void Initialise()
{
    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        queue[i] = zero_data;
    }

    front = 0;
    back = 0;
    queue_len = 0;
}

// ========================================
// Print queue to screen
// ========================================
void ShowYourself()
{
    printf("\n");

    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        printf("\t(%c, %i)", queue[i].value, queue[i].priority);
    }
    
    printf("\n");
}

// ========================================
// Checks if queue is full by comparing
// queue length with max queue size
// ========================================
bool isFull()
{
    if (queue_len == MAX_QUEUE_SIZE)
    {
        return true;
    }

    return false;
}

// ========================================
// Inserts new data item into queue at
// given position and shifts all elements
// with a lower priority back one place
// ========================================
void insert(dataItem new_data, int position)
{
    if (queue_len != 0)
    {
        for (int i = position; i <= (position + queue_len); i++)
        {
            queue[(i + 1) % MAX_QUEUE_SIZE] = queue[i];
        }
    }

    queue[position] = new_data;
    queue_len++;
    printf("\n(%c, %i) inserted at position %i\n", new_data.value, new_data.priority, position);
}

// ========================================
// Adds new item to queue depending on its
// level of priority compared to other
// items already in the list
// ========================================
void enqueue(dataItem new_data)
{
    if (!isFull())
    {
        if (queue_len == 0)
        {
            insert(new_data, front);
        }
        else
        {
            for (int i = queue_len; i >= 0; i--)
            {
                if (new_data.priority <= queue[front + i].priority)
                {
                    insert(new_data, front + i + 1);
                    break;
                }
            }
        }

        if (queue_len < MAX_QUEUE_SIZE)
        {
            back = (back + 1) % MAX_QUEUE_SIZE;
        }
    }
    else
    {
        printf("\nSorry, the queue is full :(\n");
    }
}

// ========================================
// Checks if queue is empty by comparing
// queue length with zero
// ========================================
bool isEmpty()
{
    if (queue_len == 0)
    {
        return true;
    }

    return false;
}

// ========================================
// Removes item from front of queue
// Increments front marker if queue is not
// now empty
// ========================================
char dequeue()
{
    char data_value = '-';
    if (!isEmpty())
    {
        data_value = queue[front].value;
        queue[front] = zero_data;
        queue_len--;

        if (front != back)
        {
            front = (front + 1) % MAX_QUEUE_SIZE;
        }
    }
    else
    {
        printf("\nSorry, the queue is empty :(\n");
    }

    return data_value;
}

// ========================================
// Main program
// ========================================
void main()
{
    Initialise();
    ShowYourself();

    dataItem d1 = {'a', 1};
    dataItem d2 = {'b', 1};
    dataItem d3 = {'c', 2};
    dataItem d4 = {'d', 2};

    enqueue(d3);
    ShowYourself();

    enqueue(d2);
    ShowYourself();

    dequeue();
    ShowYourself();

    dequeue();
    ShowYourself();

    enqueue(d3);
    ShowYourself();

    enqueue(d4);
    ShowYourself();

    enqueue(d1);
    ShowYourself();
    
    dequeue();
    ShowYourself();

    enqueue(d3);
    ShowYourself();

    enqueue(d3);
    ShowYourself();
}