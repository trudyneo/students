using System;

namespace Hashing
{
    class HashEntry
    {
        private int key;
        private string data;

        // constructor
        public HashEntry(int given_key, string given_data)
        {
            key = given_key;
            data = given_data;
        }

        // getters
        public int get_key()
        {
            return key;
        }

        public string get_data()
        {
            return data;
        }
    }

    class HashTable
    {
        const int table_size = 10; 

        HashEntry[] hash_table;

        public HashTable()
        {
            hash_table = new HashEntry[table_size];
            
            for (int i = 0; i < hash_table.Length; i++)
            {
                hash_table[i] = null;
            }
        }

        // method to add the ordinal values the chars in the id string together.
        private int GenerateHashKey(string id)
        {
            int return_value = 0;

            for (int i = 0; i < id.Length; i++)
            {
                return_value += id[i];
            }
            return return_value % table_size;
        }

        private bool HaveSpace()
        {
            bool return_value = false;

            for (int i = 0; i < hash_table.Length; i++)
            {
                if (hash_table[i] == null || hash_table[i].get_data() == "DELETED")
                {
                    return_value = true;
                }
            }
            return return_value;
        }

        public void Insert(string data)
        {
            if (HaveSpace() == false)
            {
                Console.WriteLine("\n\tThe hash table is full " + data + " is rejected!\n");
                return;
            }

            int hash_key = GenerateHashKey(data);

            // while the slot is occupied by some other data - move on one
            while (hash_table[hash_key] != null && hash_table[hash_key].get_data() != data && hash_table[hash_key].get_data() != "DELETED")
            {
                hash_key = (hash_key + 1) % table_size; // % to wrap round!                        
            }

            // found a null free space
            hash_table[hash_key] = new HashEntry(hash_key, data);
        }

        public int Locate(string data)
        {
            int hash_key = GenerateHashKey(data);
            Console.WriteLine("\n\thash key: " + hash_key);

            // if an empty slot return -1
            if (hash_table[hash_key] == null)
            {
                return -1;
            }

            // while the slot is occupied by some other data - move on one
            while (hash_table[hash_key] != null && hash_table[hash_key].get_data() != data)
            {
                hash_key = (hash_key + 1) % table_size; // % to wrap round!                        
            }

            // got it
            return hash_key;
        }

        // ***************************************************
        // modification of above function to return boolean
        // value depending on whether the string ID is in the
        // hash table or not
        // ***************************************************
        public bool LocateStringID(string id)
        {
            int hash_key = GenerateHashKey(id);
            Console.WriteLine("\n\thash key: " + hash_key);

            // if an empty slot return -1
            if (hash_table[hash_key] == null)
            {
                return false;
            }

            // while the slot is occupied by some other data - move on one
            while (hash_table[hash_key] != null && hash_table[hash_key].get_data() != id)
            {
                hash_key = (hash_key + 1) % table_size; // % to wrap round!                        
            }

            // got it
            return true;
        }

        public void Delete(string data)
        {
            int location = Locate(data);

            if(location == -1)
            {
                Console.WriteLine("The hash table does not contain the data " + data);
            }
            else
            {
                hash_table[location] = new HashEntry(location, "DELETED");
            }
        }
       
        public void DisplayTable()
        {
            Console.WriteLine("\n\tHash Table:");
            Console.WriteLine("\t==========");

            for (int i = 0; i < hash_table.Length; i++)
            {
                if (hash_table[i] == null)
                {
                    Console.WriteLine("\t[" + i + "] = []");
                }
                else
                {
                    Console.WriteLine("\t[" + i + "] = " + hash_table[i].get_data());
                }
            }
        }
    }
}
