﻿using System;

namespace Hashing
{
    class Program
    {
        static void Main(string[] args)
        {
            OpenHashTable table = new OpenHashTable();

            table.Insert("MGY");
            table.Insert("ANN");
            table.Insert("LNN");
            table.Insert("TPL");
            table.Insert("NSS");
            table.Insert("GHO");
            table.Insert("KCA");
            table.Insert("YGM");

            table.DisplayTable();
            
            int hash_table_index = table.Locate("ANN");      
            Console.WriteLine("\n\tANN has been located at index: " + hash_table_index);

            hash_table_index = table.Locate("TPL");      
            Console.WriteLine("\tTPL has been located at index: " + hash_table_index);

            hash_table_index = table.Locate("KCA");      
            Console.WriteLine("\tKCA has been located at index: " + hash_table_index);
            Console.WriteLine();
        }
    }
}
