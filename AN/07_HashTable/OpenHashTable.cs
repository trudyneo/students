using System;

namespace Hashing
{
    class HashNode
    {
        private int key;
        private string data;
        HashNode next;

        // constructor
        public HashNode(int given_key, string given_data)
        {
            key = given_key;
            data = given_data;
            next = null;
        }

        // getters
        public int getKey()
        {
            return key;
        }

        public string getData()
        {
            return data;
        }

        public HashNode getNextNode()
        {
            return next;
        }

        // setters
        public void setNextNode(HashNode item)
        {
            next = item;
        }
    }

    class OpenHashTable
    {
        const int table_size = 10;
        HashNode[] hash_table;

        public OpenHashTable()
        {
            hash_table = new HashNode[table_size];

            for (int i = 0; i < hash_table.Length; i++)
            {
                hash_table[i] = null;
            }
        }

        // method to add the ordinal values the chars in the id string together.
        private int GenerateHashKey(string id)
        {
            int return_value = 0;

            for (int i = 0; i < id.Length; i++)
            {
                return_value += id[i];
            }
            return return_value % table_size;
        }

        public void Insert(string data)
        {
            int hash_key = GenerateHashKey(data);

            HashNode new_item = new HashNode(hash_key, data);
            HashNode current_node = hash_table[hash_key];

            // if the cell in the table is empty, put the node in as normal
            if (current_node == null)
            {
                hash_table[hash_key] = new_item;
                return;
            }
            // otherwise create a linked list if the address is pre-occupied with another item
            else            
            {
                while (current_node.getNextNode() != null)
                {
                    current_node = current_node.getNextNode();
                }
                current_node.setNextNode(new_item);
            }
        }

        public int Locate(string data)
        {
            int hash_key = GenerateHashKey(data);
            Console.WriteLine("\n\thash key: " + hash_key);

            // if an empty slot return -1
            if (hash_table[hash_key] == null)
            {
                return -1;
            }

            // got it
            return hash_key;
        }

        public void DisplayTable()
        {
            Console.WriteLine("\n\tHash Table:");
            Console.WriteLine("\t==========");

            HashNode current_node = null;
            for (int i = 0; i < hash_table.Length; i++)
            {
                current_node = hash_table[i];
                if (current_node == null)
                {
                    Console.WriteLine("\t[" + i + "] = []");
                }
                else
                {
                    Console.Write("\t[" + i + "] = ");
                    while (current_node != null)
                    {
                        Console.Write(current_node.getData() + " ");
                        current_node = current_node.getNextNode();
                    }
                    Console.WriteLine();
                }
            }
        }
    }   
}