// g++ -o LinkedList LinkedListAlliyah.cpp
// ./LinkedList
#include <linux/limits.h>
#include <iostream>
#include <cstring>

class Node
{
public:
    int priority;
    char name[NAME_MAX];
    Node* pNext;
};

Node* pHead = NULL;

void ShowYourself()
{
    Node* pListRunner = pHead;

    printf("\n\nThe current list state is:\n");
    printf("\nName:\t\tPriority:\n");

    while (pListRunner != NULL)
    {
        printf("\n%s\t\t%i", pListRunner->name, pListRunner->priority);
        pListRunner = pListRunner->pNext;
    }
}

void add_to_front(char name_to_add[NAME_MAX], int priority_of_name)
{
    // allocate dynamic memory (from the heap) for the new node
    Node* pNewNode = new Node();

    // initialise the node
    pNewNode->priority = priority_of_name;
    strcpy(pNewNode->name, name_to_add);

    // New node points to the current head of the list
    pNewNode->pNext = pHead;
    // Head of the list points to the new node
    pHead = pNewNode;
}

void add_to_back(char name_to_add[NAME_MAX], int priority_of_name) 
{
    // allocate dynamic memory (from the heap) for the new node
    Node* pNewNode = new Node();

    // initialise the node
    pNewNode->priority = priority_of_name;
    strcpy(pNewNode->name, name_to_add);

    // Check if the list is empty
    if (pHead == NULL)
    {
        pHead = pNewNode;
    }
    else
    {
        Node* pListRunner = pHead;
        while (pListRunner->pNext != NULL)
        {
            pListRunner = pListRunner->pNext;
        }
        pListRunner->pNext = pNewNode;
    }
}

bool remove_name(char name_to_remove[NAME_MAX])
{
    // Check if the list is empty
    if (pHead == NULL)
    {
        return false;
    }

    // Start from the head of the list
    Node* pListRunner = pHead;
    Node* pPrevious = NULL;

    bool found = false;   

    // If the element to delete is at the head
    if (strcmp(pListRunner->name, name_to_remove) == 0)
    {
        // Free the heap memory, like good citizens
        delete(pHead);
        // Move the head to point to the next node  
        pHead = pListRunner->pNext;
        return true;
    }
    else
    {
        // Go through the elements of the list until you find the node
        // or reach the end of the list
        while (found == false && pListRunner != NULL)
        {
            // Stop when you find the node to delete
            if (strcmp(pListRunner->name, name_to_remove) == 0)
                found = true;
            else
            {
                pPrevious = pListRunner;            // Keep track of the previous node
                pListRunner = pListRunner->pNext;   // Move to the next node        
            }
        }
    }
    // If you reached the end of the list and did not find the element
    if (pListRunner == NULL && found == false)
    {
        return false;
    }

    // make the previous node point to the node
    // that comes after the node to delete
    pPrevious->pNext = pListRunner->pNext;

    // Free the heap memory, like good citizens
    delete(pListRunner);
    return true;
}

bool remove_priority(int priority)
{
    // Check if the list is empty
    if (pHead == NULL)
    {
        return false;
    }

    // Start from the head of the list
    Node* pListRunner = pHead;
    Node* pPrevious = NULL;

    bool found = false;   

    // If the element to delete is at the head
    if (pListRunner->priority == priority)
    {
        // Free the heap memory, like good citizens
        delete(pHead);
        // Move the head to point to the next node  
        pHead = pListRunner->pNext;
        return true;
    }
    else
    {
        // Go through the elements of the list until you find the node
        // or reach the end of the list
        while (found == false && pListRunner != NULL)
        {
            // Stop when you find the node to delete
            if (pListRunner->priority == priority)
                found = true;
            else
            {
                pPrevious = pListRunner;            // Keep track of the previous node
                pListRunner = pListRunner->pNext;   // Move to the next node        
            }
        }
    }
    // If you reached the end of the list and did not find the element
    if (pListRunner == NULL && found == false)
    {
        return false;
    }

    // make the previous node point to the node
    // that comes after the node to delete
    pPrevious->pNext = pListRunner->pNext;

    // Free the heap memory, like good citizens
    delete(pListRunner);
    return true;
}

// *********************************************
//                ALLIYAH'S CODE
// *********************************************
bool isEmpty()
{
    if (pHead == NULL)
    {
        return true;
    }
    return false;
}

void pop()
{   
    if (isEmpty())
    {
        printf("\nSorry this list is empty :(");
    }

    // Start from the head of the list
    Node* pListRunner = pHead;
    Node* pPrevious = NULL;

    if (pListRunner->pNext == NULL)
    {
        printf("\n\nPopped: %s", (char*)pListRunner->name);
        // Free the heap memory
        delete(pHead);
        // Set the head to point to...nothing
        pHead = NULL;
    }
    else
    {
        // Go through all elements until you reach the end of the list
        while (pListRunner->pNext != NULL)
        {
            pPrevious = pListRunner;           // Keep track of previous node
            pListRunner = pListRunner->pNext;  // Move to next node

        }

        // Make previous node point to node after the last node
        pPrevious->pNext = pListRunner->pNext;

        printf("\n\nPopped: %s", (char*)pListRunner->name);

        // Free the heap memory
        delete(pListRunner);
    }
}

int list_length()
{
    // If the list is empty, then there are no elements in the list...
    if (isEmpty())
    {
        return 0;
    }

    // Prepare to find the length of the list
    int length = 0;
    Node* pListRunner = pHead;

    // Go through the elements of list until you reach the last node
    while (pListRunner != NULL)
    {
        pListRunner = pListRunner->pNext;  // Move to the next node
        length++;                          // Increment length
    }

    // Free the heap memory
    delete(pListRunner);

    return length;
}

void insert(char name_to_add[NAME_MAX], int priority, int position)
{
    // If the list is empty...
    if (isEmpty())
    {
        // ...just add the element to the front of the list (it's easier)
        add_to_front(name_to_add, priority);
    }
    // If the user puts in a position that is outside the index range of the list...
    else if (position > (list_length() - 1))
    {
        // ...just add the element to the back of the list 
        add_to_back(name_to_add, priority);
    }
    // Otherwise the user wants to add the name in the middle of the list somewhere
    else
    {
        // Initialise the nodes
        Node* pNewNode = new Node();
        Node* pListRunner = pHead;
        Node* pPrevious = NULL;

        pNewNode->priority = priority;
        strcpy(pNewNode->name, name_to_add);

        // Iterate over the list as long as the i is less than the position to insert
        for (int i = 0; i < position; i++)
        {
            pPrevious = pListRunner;           // Keep track of previous node
            pListRunner = pListRunner->pNext;  // Move to the next node
        }

        pPrevious->pNext = pNewNode;    // Link the next previous node to the new node
        pNewNode->pNext = pListRunner;  // Link the new node to the one after it
    }
    
}

int main()
{
    insert((char*)"Kenneth", 10, 10);
    ShowYourself();

    add_to_back((char*)"Leslie", 15);
    ShowYourself();

    insert((char*)"Barbara", 12, 1);
    ShowYourself();

    pop();
    ShowYourself();

    insert((char*)"Sidney", 2, 1);
    ShowYourself();
}