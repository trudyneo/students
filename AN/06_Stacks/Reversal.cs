//*******************************************
// The code in this file is to be used if the
// code in the Program.cs file is not correct
//*******************************************

using System;

namespace Queue_Stack
{
    class Reversal
    {
        Queue q = new Queue();
        Stack s = new Stack();
        public Reversal()
        {
            addQueueItems();
            emptyQueueFillStack();
            reverse();
        }

        void addQueueItems()
        {
            int i = 0;
            while (!q.isFull())
            {
                q.enQueue(i);
                i++;
            }

            q.ShowYourself();
        }

        void emptyQueueFillStack()
        {
            int item;
            while (!q.isEmpty())
            {
                item = q.deQueue();
                s.push(item);
            }
        }

        void reverse()
        {
            int item;
            while (!s.isEmpty())
            {
                item = s.pop();
                q.enQueue(item);
            }

            q.ShowYourself();
        }
    }
}