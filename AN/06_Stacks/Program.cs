﻿using System;

namespace Queue_Stack
{
    class Program
    {
        Queue q = new Queue();
        Stack s = new Stack();
        
        //==========================================
        // Add the items to the queue
        //==========================================
        void addQueueItems()
        {
            int i = 0;
            while (!q.isFull())
            {
                q.enQueue(i);
                i++;
            }
            
            q.ShowYourself();
        }

        //==========================================
        // Dequeue the queue and put the item 
        // removed onto the stack
        //==========================================
        void emptyQueueFillStack()
        {
            int item;
            while (!q.isEmpty())
            {
                item = q.deQueue();
                s.push(item);
            }
        }

        //==========================================
        // Pop the stack and enqueue each item one
        // by one
        //==========================================
        void reverse()
        {
            int item;
            while (!s.isEmpty())
            {
                item = s.pop();
                q.enQueue(item);
            }
            
            q.ShowYourself();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.addQueueItems();
            p.emptyQueueFillStack();
            p.reverse();
        }
    }
}
