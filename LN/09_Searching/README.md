# Binary Tree code #

### To run this project.  ###

* unzip into a folder

* open that folder in VSCode

* compile with: 

  ```
  g++ -o BinaryTree Main.cpp Node.cpp Shakespeare.cpp BinaryTree.cpp
  ```

* run with: 

  ```
  ./BinaryTree
  ```
