#include "Shakespeare.h"
#include <iostream>
#include <ctime>

#include <fstream>
#include <iostream>

int linearSearch(string* array, int length, string item_sought)
{
    int index = -1;
    int i = 0;

    bool found = false;

    while (i < length && found == false)
    {
        if (array[i] == item_sought)
        {
            index = i;
            found = true;
        }
        i++;
    }
    return index;
}

int binarySearch(string* array, int length, string item_sought)
{
    int index = -1;

    bool found = false;

    int first = 0;
    int last = length - 1;
    int iterations = 0;

    while (first <= last && found == false)
    {
        int midpoint = (first + last) / 2;

        if (array[midpoint] == item_sought)
        {
            index = midpoint;
            found = true;
        }
        else
        {
            if (array[midpoint] < item_sought)
            {
                first = midpoint + 1;
            }
            else
            {
                last = midpoint - 1;
            }
            iterations++;
        }
    }
    return index;
}

void  check_Harry(string* ShakespearsWordArray, int length)
{
     // initialise both arrays
    string arrayShake[3000]; // There are 3090 words in total in the Harry Potter text
    string arrayHarry[3000];

    // index pointers into each array
    int iShake = 0;
    int iHarry = 0;

    // read in the Harry Potter text - word by word
    ifstream readFile;
    readFile.open("./test_data/HarryPotter.txt");

    string singleWord;

    while (readFile >> singleWord)
    {
        // Test to see if this word was used by Shakespeare or not
        if (binarySearch(ShakespearsWordArray, length, singleWord) == -1)   // If binary Seach returns -1 than this is NOT found
        {
            // put in the harry only array
            arrayHarry[iHarry] = singleWord;
            iHarry++;
        }
        else // Found in Shakespeare
        {
            // put in the used by both array
            arrayShake[iShake] = singleWord;
            iShake++;
        }
    }
    readFile.close();

    cout << "List of words from Harry Potter that Shakespeare didn't use" << endl;

    for (int i = 0; i < iHarry; i++)
    {
        cout << arrayHarry[i] << endl;
    }
} 

int main()
{
    WorksOfShakespeare*  pCompleteWorksOfShakespeare = new WorksOfShakespeare();

    pCompleteWorksOfShakespeare->BuildTreeFromWorks();
    pCompleteWorksOfShakespeare->BuildArrayFromTree();

    string* ShakespearsWordArray = pCompleteWorksOfShakespeare->GetArray();
    int Size = pCompleteWorksOfShakespeare->GetNodeCount();

    check_Harry(ShakespearsWordArray, Size);

    delete pCompleteWorksOfShakespeare;
    return 0;
}

/*
// Leo code that gets all harry potter words and compares it to shakespeares if its in the shakespeare text then it adds it to the array called arrayShake 
// and if its not then it saves to arrayHarry does this for every item in the word array for harry. The big O notation looks to me like its O(n^2) not considering 
// the use of the binary search 
string* check_Harry(string* array, int length1, string* array2, int length2) 
//harry array, harry length shakespear array, shakespear length
{
    WorksOfShakespeare*  pCompleteWorksOfShakespeare = new WorksOfShakespeare();
    pCompleteWorksOfShakespeare->BuildTreeFromWorks();
    pCompleteWorksOfShakespeare->BuildArrayFromTree();
    pCompleteWorksOfShakespeare->PrintTreeToFile();
    string* ShakespearsWordArray = pCompleteWorksOfShakespeare->GetArray();
    int Size = pCompleteWorksOfShakespeare->GetNodeCount();

    string* arrayShake[length1];
    string* arrayHarry[length1];
    int count = 0;//counting from harry to check in shakespeare
    int count1 = 0;//checking item
    int count2 = 0;//shakespeare pointer
    int count3 = 0;//harry pointer
    while (count < length1)//checks each item
    {
        while (count1 < length2+1)
        {
            if (binarySearch(ShakespearsWordArray, Size, array[count]) != -1)
            {
                arrayShake[count2] = array[count];
                count2++;
            } 
            else if (count1 == length2 +1)
            {
                arrayHarry[count3] = array[count];
                count3++;
            }
            count1++;
        }
        count++;
    }
    return (arrayShake, arrayHarry);
}
*/