#include <iostream>
#include <fstream>
#include "BinaryTree.h"

// BinaryTree class constructor
BinaryTree::BinaryTree()
{
    pRoot = NULL; 
}

BinaryTree::~BinaryTree()
{
    delete pRoot;
}

void BinaryTree::AddToTree(string newWord)
{
    pRoot = insert(pRoot, newWord);
}

int BinaryTree::FindInTree(string item_sought)
{
    return search(pRoot, item_sought);
}

int BinaryTree::search(Node* pRoot, string item_sought)
{
    cout << pRoot->m_sWord << endl;

    if (pRoot == NULL)
    {
        return -1; 
    }
    else
    {
        if (item_sought == pRoot->m_sWord)
            return pRoot->m_nInstances;
        else if (item_sought < pRoot->m_sWord)
            return search(pRoot->m_pLeft, item_sought);
        else // (item_sought > pRoot->m_sWord)            
            return search(pRoot->m_pRight, item_sought);
    }
}

Node* BinaryTree::insert(Node* pRoot, string newWord)
{
    if (pRoot == NULL)
    {
        pRoot = new Node(newWord);
    }
    else
    {
        if (newWord == pRoot->m_sWord)
            pRoot->m_nInstances++;
        else if (newWord < pRoot->m_sWord)
            pRoot->m_pLeft = insert(pRoot->m_pLeft, newWord);
        else if (newWord > pRoot->m_sWord)
            pRoot->m_pRight = insert(pRoot->m_pRight, newWord);
    }
    return pRoot;
}

void BinaryTree::DisplayTreeInOrder()
{
    traverse_in_order(pRoot);
}

void BinaryTree::ArrayFromTree(string* pArray, int* pIndex)
{
    traverse_in_order(pRoot, pArray, pIndex);
}

void BinaryTree::PrintTreeToFile(ofstream* pSortedFile)
{
    traverse_in_order(pRoot, pSortedFile);
}

void BinaryTree::count_nodes(Node* pRoot, int* pCounter)
{
    if (pRoot == NULL)
    {
        return;
    }

    count_nodes(pRoot->m_pLeft, pCounter);
    (*pCounter)++;
    count_nodes(pRoot->m_pRight, pCounter);
}

int BinaryTree::GetNumberOfNodes()
{
    int count = 0;
    count_nodes(pRoot, &count);
    return count;
}

void BinaryTree::traverse_in_order(Node* pRoot, string* pArray, int* pIndex)
{
    if (pRoot == NULL)
    {
        return;
    }

    traverse_in_order(pRoot->m_pLeft, pArray, pIndex);
    pArray[*pIndex] = pRoot->m_sWord;
    (*pIndex)++;
    traverse_in_order(pRoot->m_pRight, pArray, pIndex);
}

void BinaryTree::traverse_in_order(Node* pRoot, ofstream* pSortedFile)
{
    if (pRoot == NULL)
    {
        return;
    }

    traverse_in_order(pRoot->m_pLeft, pSortedFile);
    *pSortedFile << pRoot->m_nInstances << '\t' << pRoot->m_sWord <<'\n';
    traverse_in_order(pRoot->m_pRight, pSortedFile);
}

void BinaryTree::traverse_in_order(Node* pRoot)
{
    if (pRoot == NULL)
    {
        return;
    }
    traverse_in_order(pRoot->m_pLeft);
    cout << pRoot->m_nInstances << '\t' << pRoot->m_sWord <<'\n';
    traverse_in_order(pRoot->m_pRight);
}