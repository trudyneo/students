﻿using System;

namespace Queue_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue q = new Queue();
            Stack s = new Stack();

//adds all elements to queue
            int i = 5;
            while (i > 0)
            {
                q.enQueue(i);
                i--;
            }
            q.ShowYourself();
            Console.WriteLine("Before edited");

//transfers and removes from queue to stack
            int j = 0;
            while (j <= 4)
            {
                s.push(q.deQueue());
                j++;
            }
            s.ShowYourself();
//transfers back to queue from stack
            int l = 0;
            while (l <= 4)
            {
                q.enQueue(s.pop());
                l++;
            }
            q.ShowYourself();



        }
    }
}
