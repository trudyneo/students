
import random

print("You are playing rock paper scissors")

def AI(User_answer, bot_answer):
    if bot_answer == User_answer:
        print("its a tie")
    else:
        if User_answer == 1:
            if bot_answer == 2:
                print("bot wins")
            else: 
                print("you win")
        if User_answer == 2:
            if bot_answer == 3:
                print("bot wins")
            else: 
                print("you win")
        if User_answer == 3:
            if bot_answer == 1:
                print("bot wins")
            else: 
                print("you win")

num1 = 0
num2 = 1

def answer(bot_answer):
    if bot_answer == 1:
        print("The bot chose rock" )
    if bot_answer == 2:
        print("The bot chose paper" )
    else: 
        print("The bot chose scissors" )

while num1 < num2:
    User_answer = input(str("input rock paper or scissors (as R, P or S): "))
    bot_answer = random.randint(1, 3)
    answer(bot_answer)
    if User_answer == "R" or User_answer == "r":
        number = 1
        AI(1, bot_answer)
        num1 += 1
        num2 += 1
    elif User_answer == "P" or User_answer ==  "p":
        number = 2
        AI(2, bot_answer)
        num1 += 1
        num2 += 1
    elif User_answer == "S" or User_answer == "s":
        number = 3
        AI(3, bot_answer)
        num1 += 1
        num2 += 1
    else:
        print("input R, P or S")
        num1 += 1
        num2 += 1