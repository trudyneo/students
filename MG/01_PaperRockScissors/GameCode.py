import random

player1=str(input("enter your name here please."))

def play():
    x=1
    while x!=0:
        choice=str(input("please enter your choice: rock paper scissors\n(scissors needs the s at the end)"))
        choice=choice.lower()
        if choice=="rock":
            x=0
        elif choice=="paper":
            x=0
        elif choice=="scissors":
            x=0
        else:
            print("Error, try rewriting that again please.")
    
    array=["rock","paper","scissors"]
    randint=random.randint(0,2)
    AI=array[randint]
    print(">>The computer chose: ",AI)
    
    if AI=="rock" and choice=="paper":
        print("Well done! You won!")
    elif AI=="rock" and choice=="rock":
        print("Its a tie")
    elif AI=="rock" and choice=="scissors":
        print("Too bad. The computer wins")
        
    elif AI=="paper" and choice=="paper":
        print("Its a tie!")
    elif AI=="paper" and choice=="rock":
        print("Too bad. The computer wins")
    elif AI=="paper" and choice=="scissors":
        print("Well done! You won!")

    elif AI=="scissors" and choice=="paper":
        print("Too bad. The computer wins")
    elif AI=="scissors" and choice=="rock":
        print("Well done! You won!")
    elif AI=="scissors" and choice=="scissors":
        print("Its a tie!")

    again=input("Do you wish to play again?\n(yes or no)")
    again=again.lower()
    if again=="yes":
        play()      
    
    
play()

