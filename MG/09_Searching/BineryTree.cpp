#include <iostream>
#include <fstream>
#include "BineryTree.h"

// BineryTree class constructor
BineryTree::BineryTree()
{
    pRoot = NULL; 
}

BineryTree::~BineryTree()
{
    delete pRoot;
}

void BineryTree::AddToTree(string newWord)
{
    pRoot = insert(pRoot, newWord);
}

Node* BineryTree::insert(Node* pRoot, string newWord)
{
    if (pRoot == NULL)
    {
        pRoot = new Node(newWord);
    }
    else
    {
        if (newWord == pRoot->m_sWord)
            pRoot->m_nInstances++;
        else if (newWord < pRoot->m_sWord)
            pRoot->m_pLeft = insert(pRoot->m_pLeft, newWord);
        else if (newWord > pRoot->m_sWord)
            pRoot->m_pRight = insert(pRoot->m_pRight, newWord);
    }
    return pRoot;
}

void BineryTree::DisplayTree()
{
    traverse_in_order(pRoot);
}

void BineryTree::ArrayFromTree(string* pArray, int* pIndex)
{
    traverse_in_order(pRoot, pArray, pIndex);
}

void BineryTree::PrintTreeToFile(ofstream* pSortedFile)
{
    traverse_in_order(pRoot, pSortedFile);
}

void BineryTree::count_nodes(Node* pRoot, int* pCounter)
{
    if (pRoot == NULL)
    {
        return;
    }

    count_nodes(pRoot->m_pLeft, pCounter);
    (*pCounter)++;
    count_nodes(pRoot->m_pRight, pCounter);
}

int BineryTree::GetNumberOfNodes()
{
    int count = 0;
    count_nodes(pRoot, &count);
    return count;
}

void BineryTree::traverse_in_order(Node* pRoot, string* pArray, int* pIndex)
{
    if (pRoot == NULL)
    {
        return;
    }

    traverse_in_order(pRoot->m_pLeft, pArray, pIndex);
    pArray[*pIndex] = pRoot->m_sWord;
    (*pIndex)++;
    traverse_in_order(pRoot->m_pRight, pArray, pIndex);
}

void BineryTree::traverse_in_order(Node* pRoot, ofstream* pSortedFile)
{
    if (pRoot == NULL)
    {
        return;
    }

    traverse_in_order(pRoot->m_pLeft, pSortedFile);
    *pSortedFile << pRoot->m_nInstances << '\t' << pRoot->m_sWord <<'\n';
    traverse_in_order(pRoot->m_pRight, pSortedFile);
}

void BineryTree::traverse_in_order(Node* pRoot)
{
    if (pRoot == NULL)
    {
        return;
    }
    traverse_in_order(pRoot->m_pLeft);
    cout << pRoot->m_nInstances << '\t' << pRoot->m_sWord <<'\n';
    traverse_in_order(pRoot->m_pRight);
}