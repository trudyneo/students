#include "Shakespeare.h"
#include <iostream>
#include <ctime>

#include <fstream>
#include <iostream>

int linearSearch(string* array, int length, string item_sought)
{
    int index = -1;
    int i = 0;

    bool found = false;

    while (i < length && found == false)
    {
        if (array[i] == item_sought)
        {
            index = i;
            found = true;
        }
        i++;
    }
    return index;
}

int binarySearch(string* array, int length, string item_sought)
{
    int index = -1;

    bool found = false;

    int first = 0;
    int last = length - 1;

    while (first <= last && found == false)
    {
        int midpoint = (first + last) / 2;

        if (array[midpoint] == item_sought)
        {
            index = midpoint;
            found = true;
        }
        else
        {
            if (array[midpoint] < item_sought)
            {
                first = midpoint + 1;
            }
            else
            {
                last = midpoint - 1;
            }
        }
    }
    return index;
}

void  check_Harry(string* ShakespearsWordArray, int length)
{
     // initialise both arrays
    string arrayShake[3000]; // There are 3090 words in total in the Harry Potter text
    string arrayHarry[3000];

    // index pointers into each array
    int iShake = 0;
    int iHarry = 0;

    // read in the Harry Potter text - word by word
    ifstream readFile;
    readFile.open("./test_data/HarryPotter.txt");

    string singleWord;

    while (readFile >> singleWord)
    {
        // Test to see if this word was used by Shakespeare or not
        if (binarySearch(ShakespearsWordArray, length, singleWord) == -1)   // If binary Seach returns -1 then this is NOT found
        {
            // put in the harry only array
            arrayHarry[iHarry] = singleWord;
            iHarry++;
        }
        else // Found in Shakespeare
        {
            // put in the used by both array
            arrayShake[iShake] = singleWord;
            iShake++;
        }
    }
    readFile.close();

    cout << "List of words from Harry Potter that Shakespeare didn't use" << endl;

    for (int i = 0; i < iHarry; i++)
    {
        cout << arrayHarry[i] << endl;
    }
} 

int main()
{
    WorksOfShakespeare*  pCompleteWorksOfShakespeare = new WorksOfShakespeare();

    pCompleteWorksOfShakespeare->BuildTreeFromWorks();
    pCompleteWorksOfShakespeare->BuildArrayFromTree();

    string* ShakespearsWordArray = pCompleteWorksOfShakespeare->GetArray();
    int Size = pCompleteWorksOfShakespeare->GetNodeCount();

    check_Harry(ShakespearsWordArray, Size);

    delete pCompleteWorksOfShakespeare;
    return 0;
}