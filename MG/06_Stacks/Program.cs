﻿using System;

namespace Queue_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue n = new Queue();
            Stack m = new Stack();
            n.enQueue(1);
            n.enQueue(2);
            n.enQueue(3);
            n.enQueue(4);
            n.enQueue(5);
            n.ShowYourself();

            for (int i = 0; i < 5; i++)
            {
                int holding = n.deQueue();
                m.push(holding);
                m.ShowYourself();
            }
         
            for (int i = 0; i < 5; i++)
            {
                n.deQueue();
             }
            
            for (int i = 0; i < 5; i++)
            {
                int temp=m.pop();
                n.enQueue(temp);
                n.ShowYourself();
            }         
        }
    }
}
