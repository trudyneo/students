using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] number_array = new int[9]; 
                        
            SortingAlgorithms algors = new SortingAlgorithms();

            MuddleArray(number_array);
            algors.BubbleSort(number_array);
            MuddleArray(number_array);
            algors.BubbleSort_II(number_array);
            MuddleArray(number_array);
            algors.InsertionSort(number_array);

            Console.WriteLine("MergeSort");
            MuddleArray(number_array);
            algors.MergeSort(number_array);
        }

        static void MuddleArray(int [] number_array)
        {
            if (number_array.Length == 9)
            {
                number_array[0] = 5;
                number_array[1] = 3;
                number_array[2] = 2;
                number_array[3] = 7;
                number_array[4] = 70;
                number_array[5] = 9;
                number_array[6] = 1;
                number_array[7] = 3;
                number_array[8] = 8;
            }
        }

        static void RandomiseArray(int[] number_array)
        {
            Random rand = new Random();

            if (number_array.Length == 7)
            {
                for(int i = 0; i <  number_array.Length; i++)
                {
                    number_array[i] = rand.Next(0,100);
                }                
            }
        }
    }
}
