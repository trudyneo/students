using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sorting
{
    class SortingAlgorithms
    {
        public void BubbleSort(int[] number_array)
        {
            Console.WriteLine("BubbleSort");

            print(number_array);                    

            int num_items = number_array.Length;
            int temp_int = 0;

            for (int i = 0; i <= num_items - 2; i++)
            {
                for (int j = 0; j <= num_items - i - 2; j++)
                {
                    // Check for need to swap
                    if (number_array[j] > number_array[j + 1])
                    {
                        // swap
                        temp_int = number_array[j];
                        number_array[j] = number_array[j + 1];
                        number_array[j + 1] = temp_int;
                    }
                }
                print(number_array);                    
            }       
        }  

        public void BubbleSort_II(int[] number_array)
        {
            Console.WriteLine("BubbleSort_II");
            print(number_array);                    

            int num_items = number_array.Length;
            bool flag = true;
            int temp_int = 0;
            int i = 0;

            while (i < num_items - 1 && flag == true)
            {
                flag = false;
                for (int j = 0; j <= num_items - i - 2; j++)
                {
                    // Check for need to swap
                    if (number_array[j] > number_array[j + 1])
                    {
                        // swap
                        temp_int = number_array[j];
                        number_array[j] = number_array[j + 1];
                        number_array[j + 1] = temp_int;
                        flag = true;
                    }
                }
                print(number_array);    
                i++;                
            }
        }  

        public void InsertionSort(int[] number_array)
        {
            Console.WriteLine("InsertionSort");
            print(number_array);                    

            int num_items = number_array.Length;

            for (int i = 1; i < num_items; i++)
            {
                int current_value = number_array[i];
                int position = i;
                while (position > 0 && number_array[position -1] > current_value)
                {
                    // move the number larger then current_value to the right 
                    number_array[position] = number_array[position -1];
                    // make our way to left
                    position--;
                }
                number_array[position] = current_value;
                print(number_array);    
            }
        }

        public void MergeSort(int[] number_array)
        {
            print(number_array);
            int num_items = number_array.Length;
      
            if (num_items <= 1)
                return;
            
            int middle = num_items  /  2;

            int[] left = new int[middle + 1];
            int[] right = new int[middle + 1];

            int l = 0; 
            for (l = 0; l < middle; l++) 
            {
                left[l] = number_array[l];
            }
            Array.Resize(ref left, l);

            for ( l = middle; l < num_items; l++)
            {
                right[l - middle] = number_array[l];
            }
            Array.Resize(ref right, l - middle);

            MergeSort(left);
            MergeSort(right);

            int i = 0;
            int j = 0;
            int k = 0;

            while (i < left.Length && j < right.Length)
            {
                if (left[i] < right[j])
                {
                    number_array[k] = left[i];
                    i = i + 1;
                }
                else
                {
                    number_array[k] = right[j];
                    j = j + 1;
                }
                k = k + 1;
                print(number_array);
            }

            while (i < left.Length)
            {
                number_array[k] = left[i];
                i = i + 1;
                k = k + 1;
                print(number_array);
            }

            while (j < right.Length)
            {
                number_array[k] = right[j];
                j = j + 1;
                k = k + 1;
                print(number_array);
            }
            print(number_array);
        }

        void print(int[] number_array)
        {
            Console.Write("\t [ ");

            foreach (int i in number_array)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine("]");
        }      
    }
}
