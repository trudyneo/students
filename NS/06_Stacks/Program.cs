﻿using System;

namespace Queue_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue qu = new Queue(); 
            qu.enQueue(8);
            qu.enQueue(7);
            qu.enQueue(6);
            qu.enQueue(4);
            qu.enQueue(1);
            qu.ShowYourself();
            qu.reverse();
            qu.ShowYourself();
        }
    }
}
