from random import randint
pl1choice = ""
pl2choice = ""
vshuman = False
if input("Are there 2 players?") == "yes":
  vshuman = True
decide = "RSPR"
playing = True
p1tot = 0
p2tot = 0
while playing:
  print("\n"*40)
  pl1choice = (input("player 1's turn:\n")[0]).upper()
  print("\n"*40)
  if vshuman:
    pl2choice = (input("player 2's turn:\n")[0]).upper()
    print("\n"*40)
  else:
    pl2choice = ["R","P","S"][randint(0,2)]
  print("player 1 played", pl1choice)
  print("player 2 played", pl2choice,"\n")
  if pl1choice+pl2choice in decide:
    print("player 1 wins!")
    p1tot+=1
  elif pl2choice+pl1choice in decide:
    print("player 2 wins!")
    p2tot+=1
  else:
    print("Draw!")
  print("\nPlayer 1 has", p1tot, "points")
  print("Player 2 has", p2tot, "points")
  input("\nPress enter to continue")
