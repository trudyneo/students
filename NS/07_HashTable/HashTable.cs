using System;

namespace Hashing
{
    class HashEntry
    {
        private int key;
        private string data;

        // constructor
        public HashEntry(int given_key, string given_data)
        {
            key = given_key;
            data = given_data;
        }

        // getters
        public int get_key()
        {
            return key;
        }

        public string get_data()
        {
            return data;
        }
    }

    class HashTable
    {
        const int table_size = 10; 

        HashEntry[] hash_table;

        public HashTable()
        {
            hash_table = new HashEntry[table_size];
            
            for (int i = 0; i < hash_table.Length; i++)
            {
                hash_table[i] = null;
            }
        }

        // method to add the ordinal values the chars in the id string together.
        private int GenerateHashKey(string id)
        {
            int return_value = 0;

            for (int i = 0; i < id.Length; i++)
            {
                return_value += id[i];
            }
            return return_value % table_size;
        }

        private bool HaveSpace()
        {
            bool return_value = false;

            for (int i = 0; i < hash_table.Length; i++)
            {
                if (hash_table[i] == null)
                {
                    return_value = true;
                }
                else if (hash_table[i].get_key() == -1)
                {
                    return_value = true;
                }
            }
            return return_value;
        }

        public void Insert(string data)
        {
            if (HaveSpace() == false)
            {
                Console.WriteLine("\n\tThe hash table is full " + data + " is rejected!\n");
                return;
            }

            int hash_key = GenerateHashKey(data);

            // while the slot is occupied by some other data - move on one
            while ((hash_table[hash_key] != null && hash_table[hash_key].get_key() != -1) && 
                    hash_table[hash_key].get_data() != data)
                    {
                        hash_key = (hash_key + 1) % table_size; // % to wrap round!                        
                    }
            
            // found a null free space
            hash_table[hash_key] = new HashEntry(hash_key, data);
        }

        public int Locate(string data)
        {
            int hash_key = GenerateHashKey(data);

            // if an emply slot return -1
            if (hash_table[hash_key] == null ^ hash_table[hash_key].get_key() == -1)
            {
                return -1;
            }

            // while the slot is occupied by some other data - move on one
            while (hash_table[hash_key] != null &&
                    hash_table[hash_key].get_data() != data)
                    {
                        hash_key = (hash_key + 1) % table_size; // % to wrap round!                        
                    }

            // got it
            return hash_key;
        }
       
        public void DisplayTable()
        {
            Console.WriteLine("\n\tHash Table:");
            Console.WriteLine("\t==========");

            for (int i = 0; i < hash_table.Length; i++)
            {
                if (hash_table[i] == null)
                {
                    Console.WriteLine("\t[" + i + "] = []");
                }
                else if (hash_table[i].get_key() == -1)
                {
                    Console.WriteLine("\t[" + i + "] = []");
                }
                else
                {
                    Console.WriteLine("\t[" + i + "] = " + hash_table[i].get_data());
                }
            }
        }

        public void Delete(string data)
        {
            int location = Locate(data);
            if (location != -1)
            {
                int hash_key = GenerateHashKey(data);
                hash_table[hash_key] = new HashEntry(-1, data);
            }
            else
            {
                Console.WriteLine(data + "\t is not in the table");
            }
        }
    }
}